<?php
/**
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EvvProductStreamImport\Setup\CustomProfiles;

/**
 * Class ProfileHelper
 */
class ProfileHelper
{
    /**
     * @return ProfileMetaData[]
     */
    public static function getProfileInstances()
    {
        return [
            new ProductStreamProfile(),
        ];
    }
}
