<?php
/**
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace EvvProductStreamImport\Setup\CustomProfiles;

use Shopware\Setup\SwagImportExport\DefaultProfiles\ProfileMetaData;
use Shopware\Components\SwagImportExport\DbAdapters\DataDbAdapter;

class ProductStreamProfile implements ProfileMetaData, \JsonSerializable
{
    const PRODUCT_STREAM_NAME = 'custom_product_stream_profile';
    const PRODUCT_STREAM_DESCRIPTION = 'custom_product_stream_profiles_description';

    /**
     * Returns the adapter of the profile.
     */
    public function getAdapter()
    {
        return 'articles';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::PRODUCT_STREAM_NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return self::PRODUCT_STREAM_DESCRIPTION;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => 'root',
            'name' => 'Root',
            'type' => 'node',
            'children' => [
                [
                    'id' => '58458e1c359eaf',
                    'name' => 'product_streams',
                    'index' => 0,
                    'type' => ''
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    /*private function getProductStreamsFields()
    {
        return [
            [
                'id' => '58458e1c01c46f',
                'type' => 'leaf',
                'index' => 1,
                'name' => 'id',
                'shopwareField' => 'id',
            ],
            [
                'id' => '58458e1c01a47f',
                'type' => 'leaf',
                'index' => 1,
                'name' => 'Column Name',
                'shopwareField' => 'column',
            ],
        ];
    }*/
}
