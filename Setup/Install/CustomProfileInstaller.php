<?php

namespace EvvProductStreamImport\Setup\Install;

use Doctrine\DBAL\Connection;
use EvvProductStreamImport\Setup\CustomProfiles\ProfileHelper;
use Shopware\Setup\SwagImportExport\DefaultProfiles\ProfileMetaData;
use Shopware\Setup\SwagImportExport\Install\InstallerInterface;
use Shopware\Setup\SwagImportExport\SetupContext;

class CustomProfileInstaller implements InstallerInterface
{
    const MIN_PLUGIN_VERSION = "0.0.1";
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var SetupContext
     */
    private $setupContext;


    public function __construct(SetupContext $setupContext, Connection $connection)
    {
        $this->connection = $connection;
        $this->setupContext = $setupContext;
    }

    /**
     * {@inheritdoc}
     */
    public function install()
    {
        $sql = '
            INSERT IGNORE INTO s_import_export_profile
            (`type`, `name`, `description`, `tree`, `hidden`, `is_default`)
            VALUES
            (:type, :name, :description, :tree, :hidden, :is_default)
        ';

        /** @var ProfileMetaData[] $profiles */
        $profiles = ProfileHelper::getProfileInstances();

        foreach ($profiles as $profile) {
            $serializedTree = json_encode($profile);

            $params = [
                'type' => $profile->getAdapter(),
                'name' => $profile->getName(),
                'description' => $profile->getDescription(),
                'tree' => $serializedTree,
                'hidden' => 0,
                'is_default' => 0,
            ];

            $this->connection->executeQuery($sql, $params);
        }
    }

    public function isCompatible()
    {
        return $this->setupContext->assertMinimumPluginVersion(self::MIN_PLUGIN_VERSION);
    }
}
