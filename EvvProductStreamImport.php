<?php

namespace EvvProductStreamImport;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Shopware\Bundle\AttributeBundle\Service\TypeMapping;
use Shopware\Setup\SwagImportExport\Install\InstallerInterface;
use Shopware\Setup\SwagImportExport\SetupContext;
use EvvProductStreamImport\Setup\Install\CustomProfileInstaller;

class EvvProductStreamImport extends Plugin
{
    public function getVersion()
    {
        #$config = $this->container->get('shopware.plugin.config_reader')->getByPluginName('EvvProductStreamImport');
        #$config = $this->container->get('shopware.plugin_xml_plugin_info_reader')->getByPluginName('EvvProductStreamImport');
        $swagExample = $this->container->get('kernel')->getPlugins()['EvvProductStreamImport'];

        return $config;
    }

    public function install(InstallContext $installContext)
    {

        $swVersion = Shopware()->Config()->get('version');
        if (version_compare($swVersion, '5.4.0', '<=')) {
            throw new \Exception('This plugin requires Shopware 5.4.0 or a later version');
        }

        /** @var CacheManager $cacheManager */
        $cacheManager = $this->container->get('shopware.cache_manager');
        $cacheManager->clearProxyCache();

        $setupContext = new SetupContext(
            $swVersion,
            $installContext->getPlugin()->getVersion(),
            SetupContext::NO_PREVIOUS_VERSION
        );

        $installers = [];
        $installers[] = new CustomProfileInstaller($setupContext, $this->container->get('dbal_connection'));

        /** @var InstallerInterface $installer */
        foreach ($installers as $installer) {
            if (!$installer->isCompatible()) {
                continue;
            }
            $installer->install();
        }

        return false;
    }
}

