<?php

namespace EvvProductStreamImport\Subscribers;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Gaufrette\File;
use Shopware\CustomModels\ImportExport\Profile;
use EvvProductStreamImport\Setup\CustomProfiles\ProductStreamProfile;
use Shopware\Models\ProductStream\ProductStream;
use Shopware\CustomModels\ImportExport\Session;
use Shopware\Models\Attribute\Configuration;

class ProductStreamSubscriber implements SubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            #'Enlight_Controller_Action_PreDispatch_Backend_Index' => 'onImportStart',
            #'Enlight_Controller_Action_PreDispatch_Backend_SwagImportExportImport' => 'onImportStart',
            'Shopware_Controllers_Backend_SwagImportExportImport::importAction::replace' => 'onImportStart',
        ];
    }

    public function onImportStart(Enlight_Event_EventArgs $args)
    {
        $controller = $args->getSubject();

        $request = $controller->Request();
        $profileId = $request->getParam('profileId', -1);
        $sessionId = (int) 1000 . $profileId;

        if ($profileId === -1) {
            throw new \Exception('Something went wrong');
        }

        $manager = $controller->get('Models');
        $profileRepository = $manager->getRepository(Profile::class);

        $profileEntity = $profileRepository->findOneBy(['id' => $profileId]);

        $profileName = $profileEntity->getName();

        if ($profileName == ProductStreamProfile::PRODUCT_STREAM_NAME) {

            /** @var UploadPathProvider $uploadPathProvider */
            $uploadPathProvider = $controller->get('swag_import_export.upload_path_provider');
            $fileName = $request->getParam('importFile');
            $inputFile = $uploadPathProvider->getRealPath($fileName);

            $unprocessedFiles = [];
            $requestData = [
                'type' => 'import',
                'profileId' => (int)$profileId,
                'importFile' => $fileName,
                'sessionId' => $request->getParam('sessionId'),
                'limit' => [],
            ];

            if ($request->getParam('unprocessedFiles')) {
                $unprocessedFiles = json_decode($request->getParam('unprocessedFiles'), true);
            }

            if (!isset($requestData['format'])) {
                // get file format
                $requestData['format'] = pathinfo($inputFile, PATHINFO_EXTENSION);
                $requestData['name'] = pathinfo($inputFile, PATHINFO_FILENAME);
            }

            if ($requestData['format'] != 'csv') {
                throw new \Exception('Only CSV import supported!');
            }

            $nameParts = explode('-', $requestData['name']);
            $fieldName = (count($nameParts) > 1) ? $nameParts[1] : $nameParts[0];
            $fieldName = str_replace('-', '_', trim($fieldName));
            $fieldType = 'string';

            $importFinished = false;

            try {
                $csvData = file($inputFile, FILE_IGNORE_NEW_LINES);


                if (!empty($csvData)){

                    $csvData = array_slice($csvData, 1);

                    $rowsCount = count($csvData);
                    if ($_SESSION['position'] >= $rowsCount){
                        $_SESSION['position'] = $rowsCount;
                        $importFinished = true;
                    }

                    $position = $_SESSION['position'];
                    $position = $position == null ? 0 : $position;

                    $resultData = [
                        'type' => 'import',
                        'profileId' => $profileId,
                        'importFile' => $fileName,
                        'sessionId' => $sessionId,
                        'limit' => [],
                        'format' => 'csv',
                        'position' => $position,
                        'unprocessedFiles' => json_encode($unprocessedFiles),
                    ];

                    $resultData['batchSize'] = Shopware()->Config()->getByNamespace('SwagImportExport', 'batch-size-import');

                    if (!$resultData['batchSize']){
                        $resultData['batchSize'] = 1;
                    }

                    if (!isset($_SESSION['position'])){
                        $_SESSION['position'] = $resultData['batchSize'];
                    } else {
                        $_SESSION['position'] += $resultData['batchSize'];
                    }

                    // TODO Check product stream attribute and create it if not exists
                    #$tableConfig = $crudService->get($table, $fieldName);
                    #$repository = $manager->getRepository(ProductStream::class);
                    #$productStreams = $repository->findAll();
                    $crudService = Shopware()->Container()->get('shopware_attribute.crud_service');
                    $entityManager = Shopware()->Models();

                    $table = 's_product_streams_attributes';
                    $crudService->update($table, $fieldName, $fieldType, [
                        'label' => 'SKU Field',
                        'supportText' => 'SKU value',
                        'helpText' => 'Product stream for SKU',
                        'translatable' => true,
                        'displayInBackend' => true,
                        'position' => 100,
                        'custom' => true,
                    ]);
                    $entityManager->generateAttributeModels(['s_product_streams_attributes']);


                    if ($position != $rowsCount){

                        $importSlice = array_slice($csvData, $position, $resultData['batchSize']);
                        foreach ($importSlice as $csvDatum) {
                            $productStream = new ProductStream();

                            $sreamData = [
                                'name' => 'Product Stream ' . $csvDatum,
                                'conditions' => null,
                                'type' => 2,
                                'sorting' => 1,
                                'description' => 'Product Stream for ' . $csvDatum . ' ' . $fieldName,
                                'sorting_id' => 1,
                            ];

                            $productStream->fromArray($sreamData);
                            $productStream->setAttribute([$fieldName => $csvDatum]);
                            $a = '';
                            $entityManager->persist($productStream);
                            $entityManager->flush($productStream);
                        }
                    }


                    $controller->View()->assign([
                        'success' => true,
                        'data' => $resultData,
                    ]);
                    if ($importFinished){
                        $_SESSION['position'] = null;
                    }
                }
            } catch (\Exception $e) {
                $controller->View()->assign([
                    'success' => false,
                    'msg' => $e->getMessage(),
                ]);
            }

        } else {
            $args->getSubject()->__executeOriginalMethod($args->getMethod());
        }
    }
}